/*
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright (c) 2023 Peter G. (nephros)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import QtQuick 2.6
import Sailfish.Silica 1.0
import Sailfish.Share 1.0
import io.thp.pyotherside 1.5

ApplicationWindow {
    id: app

    allowedOrientations: Orientation.All

    property var report
    property var uptimedinfo

    function refresh() { py.uprecords() }

    Component.onCompleted: {
        // for sailjail
        Qt.application.domain  = "sailfish.nephros.org";
        Qt.application.version = "unreleased";
        console.info("Initialized", Qt.application.name, "version", Qt.application.version, "by", Qt.application.organization );
        console.debug("Parameters: " + Qt.application.arguments.join(" "))
    }

    initialPage: mainPage
    cover: coverPage

    Component {
        id: mainPage
        Page{
            allowedOrientations: Orientation.Landscape
            PageHeader{title: "uprecords"; id: header}
            SilicaListView {
                // necessary to allow horizontal scrolling. Overridden from ListView in SilicaListView.
                //boundsBehavior: Flickable.DragOverBounds
                //flickableDirection: Flickable.AutoFlickDirection
                //quickScroll: false
                anchors.top: header.bottom
                anchors.horizontalCenter: parent.horizontalCenter
                height: parent.height-header.height
                width: parent.width - (isLandscape ? Theme.horizontalPageMargin*2 : 0)
                x: Theme.horizontalPageMargin
                model: app.report
                delegate: BackgroundItem { id: item
                    //Rectangle{ anchors.fill: parent; border.color: "red"; color: "transparent"}
                    width: ListView.view.width
                    height: lbl.height
                    anchors.margins: 0
                    hoverEnabled: true
                    highlighted: (down && isPortrait)
                    onEntered: if (isPortrait) lbl.state = "zoomed"
                    onExited: lbl.state = ""
                    Behavior on height { NumberAnimation {} }
                    Label { id: lbl
                        text:  modelData.replace(/^\ +/g, "").replace(/^up/, "up  ").replace(/^%up/, "%up ")
                        horizontalAlignment: (index <11) ? Text.AlignRight : Text.AlignLeft
                        width: parent.width
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.margins: 0
                        textFormat: Text.PlainText
                        maximumLineCount: 1
                        verticalAlignment: Text.AlignVCenter
                        font.family: "monospace"
                        font.bold: (index === 0) || (index > 14)
                        fontSizeMode: Text.HorizontalFit
                        wrapMode: Text.NoWrap
                        height: Math.max ( implicitHeight, font.pixelSize + 2 )
                        states: [
                            State { name: "zoomed"
                                PropertyChanges { target: lbl
                                    text: {
                                        const b =[]
                                        const a = modelData.trim().replace("|","").split("   ")
                                        a.forEach(function(e) {
                                            var l = e.trim()
                                            l = l.replace(/^\ */mg,"")
                                            if (l.length>1) b.push(l)
                                        })
                                        return b.join('\n')
                                    }
                                    fontSizeMode: Text.FixedSize
                                    wrapMode: Text.Wrap
                                    maximumLineCount: 10
                                    //height: Theme.itemSizeLarge
                                    horizontalAlignment: Text.AlignLeft
                                    height: implicitHeight
                                    highlighted: true
                                }
                            }
                        ]
                    }
                }
            }
        }
    }
    Component {
        id: coverPage
        CoverBackground { id: cover
            property string uptime
            property string next
            property string no1
            property string total
            property string top5
            ShareAction{ id: share  }
            onStatusChanged: {
                if (status === Cover.Activating) { app.refresh(); return }
                if (status === Cover.Active) {
                    //->  38     3 days, 06:50:35 | Linux 4.19.188 Wed Nov 29 12:53:13 2023
                    uptime = app.report[11].substr(8).trim().split('|')[0].trim()
                    next = app.report[12].split('|')[0].trim()
                    no1 =  app.report[14].split('|')[0].trim()
                    total =  app.report[15].split('|')[0].trim()
                    top5 = ''
                        + app.report[1].split('|')[0].trim() + '\n'
                        + app.report[2].split('|')[0].trim() + '\n'
                        + app.report[3].split('|')[0].trim() + '\n'
                        + app.report[4].split('|')[0].trim() + '\n'
                        + app.report[5].split('|')[0].trim() + '\n'
                }
            }
            CoverPlaceholder {
                //text: "Uprecords"
                //textColor: Theme.highlightColor
                //icon.source: "image://theme/harbour-uptimed-uprecords"
                Label { id: uptimelbl
                    y: Theme.paddingLarge
                    width: parent.width
                    //color: Theme.secondaryColor
                    horizontalAlignment: Qt.AlignHRight
                    font.pixelSize: Theme.fontSizeSmall
                    font.family: "monospace"
                    fontSizeMode: Text.Fit
                    text: uptime
                }
                Label { id: nextlbl
                    width: parent.width
                    anchors.top: uptimelbl.bottom
                    anchors.topMargin: Theme.paddingMedium
                    color: Theme.secondaryColor
                    horizontalAlignment: Qt.AlignHCenter
                    font.pixelSize: Theme.fontSizeSmall
                    font.family: "monospace"
                    fontSizeMode: Text.Fit
                    text: next + '\n' + no1 + '\n\n' + total
                }
                Label { id: toplbl
                    width: parent.width
                    //anchors.bottom: statlbl.top
                    y: coverActionArea.y - height + Theme.paddingLarge
                    anchors.topMargin: Theme.paddingMedium
                    color: Theme.secondaryColor
                    horizontalAlignment: Qt.AlignHCenter
                    font.pixelSize: Theme.fontSizeSmall
                    font.family: "monospace"
                    fontSizeMode: Text.Fit
                    text: top5
                }

                Label { id: statlbl
                    width: parent.width
                    horizontalAlignment: Qt.AlignHCenter
                    color: Theme.secondaryColor
                    font.pixelSize: Theme.fontSizeSmall
                    y: visible ? (coverActionArea.y + coverActionArea.height/2 - height/2) : parent.height
                    //anchors.horizontalCenter: coverActionArea.horizontalCenter
                    visible: text
                    //height: visible ? implicitHeight : 0
                    opacity: visible ? 1.0 : 0.0
                    Behavior on opacity { FadeAnimation{} }
                    //Behavior on height { PropertyAnimation{ duration: 900 } }
                    Behavior on y { PropertyAnimation{ duration: 900 } }
                }
                Timer {
                    running: statlbl.text
                    interval: 5000
                    onTriggered: statlbl.text = ""
                }
                CoverActionList {
                    iconBackground: true
                    CoverAction { iconSource: "image://theme/icon-s-clipboard"
                        onTriggered: {
                            Clipboard.text = app.report.join('\n')
                            statlbl.text = qsTr("copied!")
                        }
                    }
                    CoverAction { iconSource: "image://theme/icon-m-sync"; onTriggered: { app.refresh() } }
                    CoverAction { iconSource: "image://theme/icon-m-share"
                        onTriggered: {
                            var t = new Date()
                            const now =  t.toISOString()
                            share.title     = "Uprecords Report"
                            share.mimeType  = "text/plain"
                            share.resources = [
                                {
                                    "data": app.report.join('\n') + '\n' + app.uptimedinfo,
                                    "name": "Uprecords_Report_%1.txt".arg(now),
                                }
                            ]
                            share.trigger()
                        }
                    }
                }
            }
        }
    }
    Python { id: py
        Component.onCompleted: {
            // initialize and call out main thing:
            importModule('subprocess', function() {
                uprecords()
                uprecordscmd( "-v", function(r) { app.uptimedinfo = r })
            })
        }
        function uprecords() {
            call('subprocess.check_output', [ ['/usr/bin/uprecords', '-Mad'] ],
                function(r){
                    // py bytestream <==> js arraybuffer.
                    // lets just json.stringify handle the conversion from the buffer:
                    const rs= JSON.stringify(r).split('\n')
                    rs = rs.filter(function(e){ return !/^--/.test(e)})
                    rs.forEach(function(e){  return e.split("  ").join("") })
                    app.report=rs
                }
            )
        }
        function uprecordscmd(parms, callback) {
            call('subprocess.check_output', [ ['/usr/bin/uprecords', parms] ],
                function(r){
                    // py bytestream <==> js arraybuffer.
                    // lets just json.stringify handle the conversion from the buffer:
                    const rs= JSON.stringify(r).split('\n')
                    callback(rs)
                }
            )
        }
        onError: {
            console.log('Error: ' + traceback)
        }
    }

}

// vim: ft=javascript et ts=4 sw=4 st=4

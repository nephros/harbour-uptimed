# -*- mode: sh -*-

# Firejail profile for harbour-uprecords
#
### PERMISSIONS

whitelist /var/spool/uptimed/
read-only /var/spool/uptimed/
private-bin /usr/bin/uprecords
